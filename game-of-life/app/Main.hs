
{-# LANGUAGE ViewPatterns, NoMonomorphismRestriction #-}

-- | 
-- - one cell to cell interaction per cycle only when at least one of them  is alive

module Main where

import qualified Data.Set                      as S
import           Data.Set                       ( Set )
import           Control.Concurrent             ( threadDelay )
import           Data.Foldable                  ( fold
                                                , foldl'
                                                , toList
                                                )
import           Data.List                      ( unfoldr )
import           Data.Map                       ( Map )
import qualified Data.Map.Strict               as M
import           Control.Monad                  ( guard )
import           Data.Maybe                     ( fromMaybe )
import           Control.Monad.Writer
import           Control.Monad.State
import           Data.Semigroup
import           Graphics.Gloss


type Coordinates = (Int, Int)

type Universe a = Map Coordinates a
type UniverseC = Universe (Set Coordinates)

data Cell = Dead | Alive deriving (Enum, Eq)

evolve :: UniverseC -> Universe Int -> ([Coordinates], Universe Int)
evolve (M.minViewWithKey -> Just ((coo, mn), u)) r = (coo' <> coos, r'')
  where
    coo'          = guard (isAlive mn' Alive) >> pure coo
    (coos, r'')   = evolve u' r'
    (mn', u', r') = foldl' f (length mn, u, r) $ neighbours mn coo
    f (lmn, u, r) ncoo = fromMaybe new old
      where
        new = (lmn, u, M.alter (Just . maybe 1 succ) ncoo r)
        old = do
            mnn <- M.lookup ncoo u
            pure (succ lmn, M.insert ncoo (S.insert coo mnn) u, r)
evolve _ r = ([], r)

step :: UniverseC -> UniverseC
step u =
    let (as, ds) = evolve u mempty
    in  fold $ do
            c <- as <> do
                (c, n) <- M.assocs ds
                guard $ isAlive n Dead
                pure c
            pure $ bootCell c

bootCell :: Coordinates -> UniverseC
bootCell c = M.singleton c mempty

isAlive n = (== Alive) . status
  where
    status Alive | lonely n      = Dead
                 | overcrowded n = Dead
                 | otherwise     = Alive
    status Dead | warm n    = Alive
                | otherwise = Dead
    lonely      = (< 2)
    overcrowded = (> 3)
    warm        = (== 3)

neighbours :: Set Coordinates -> Coordinates -> [Coordinates]
neighbours s (x, y) = do
    dx <- [-1 .. 1]
    dy <- [-1 .. 1]
    guard $ dx /= 0 || dy /= 0
    let p = (x + dx, y + dy)
    guard $ not $ S.member p s
    pure p

--------------------------------------------------------------------------------
-- render
--------------------------------------------------------------------------------
type Width = Int
type Height = Int

mkUniverse :: [Int] -> UniverseC
mkUniverse = fold . fmap bootCell . mkPattern

mkPattern :: [Int] -> [(Int, Int)]
mkPattern []           = []
mkPattern (x : y : rs) = (x, y) : mkPattern rs

spinUniverse = [4, 5, 5, 5, 6, 5, 5, 6, 6, 6, 7, 6]
dieHard = [2, 3, 3, 2, 3, 3, 7, 2, 8, 2, 9, 2, 8, 4]
rpentomino = [2, 3, 3, 2, 3, 3, 3, 4, 4, 4]
acorn = [2, 2, 3, 2, 3, 4, 5, 3, 6, 2, 7, 2, 8, 2]


renderCell :: Coordinates -> Float -> Picture
renderCell (x,y) c =  Translate (10 * fromIntegral x) (10 * fromIntegral y) $ Color (greyN c) $ circleSolid 5

render :: Int->  (Int, UniverseC, UniverseC) -> Picture
render k (n, u, u') =
    let u'' = M.intersection u u'
    in
        Pictures
        $  do
               let c = fromIntegral (k - n) / fromIntegral k
               (p, _) <- M.assocs $ M.difference u u''
               pure $ renderCell p c
        <> do
               let c = fromIntegral n / fromIntegral k
               (p , _) <- M.assocs $ M.difference u' u''
               pure $ renderCell p c
        <> do
               (p, _) <- M.assocs $ u''
               pure $ renderCell p 1

update k vp t (n, u, u') | n >= k    = (0, u', step u')
                         | otherwise = (succ n, u, u')

bootUniverse c = (0, c', step c') where c' = mkUniverse c

main = do 
    let speed = 20 -- lower, faster
    simulate (InWindow "Nice Window" (500, 500) (10, 10))
                white
                50 -- fps
                (bootUniverse acorn)
                (render speed)
                (update speed)
