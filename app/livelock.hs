{-# LANGUAGE BlockArguments    #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}

module Main where

import           Control.Concurrent
import           Control.Concurrent.STM
import           Control.Exception
import           Control.Monad
import           Data.String.Interpolate
import           Protolude
import           Turtle
import           Data.Array




info :: Description
info = [i|
    A list of 'n' TVars each pestered by one threads applying succ to its value.
    A single thread try to read them all together and print the differential of the sum.
    Pass 'n' as argument. With n over 5 you start to see the livelock kicking in. The reader starves.
    Notice that a writer with same requirement (write to all of them atomically) would starve the same.
    The 'n' threads have the smallest requirement and they will not let the fat guy in.
    With n = 100 it's almost impossible to get in.
    If you want to see heap explode, just take away the tick on the modifyTVar' and guess why you have to kill the main thread after short :-)
    Second argument is the list of readings from the reading thread before bailing out
    |]

data Config = Config
    {   contention :: Int
    ,   readings   :: Int
    }
parseConfig = Config 
    <$> argInt "contention" "contention parameter" 
    <*> argInt "readings" "readings parameter"



main :: IO ()
main = do
    Config {..} <- options info parseConfig
    as          <- fmap
        do listArray (1, contention)
        do replicateM contention $ do
            v <- newTVarIO (0 :: Int)
            forkIO $ forever $ atomically $ modifyTVar' v succ
            pure v
    let difference :: Int -> Int -> IO ()
        difference n b 
            | n >= readings = pure ()
            | True = do 
                b' <- fmap sum $ atomically $ traverse readTVar as
                putText [i| iteration #{n}, difference: #{b' - b} |]
                difference (succ n) b'
    difference 0 0



